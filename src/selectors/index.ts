import { PipelineSchedule, PipelineStatus } from "../models";

export const getShouldStartNewPipeline = (
  pipelineSchedule: PipelineSchedule
) => {
  const lastPipeline = pipelineSchedule.last_pipeline;
  if (!lastPipeline) {
    return true;
  }
  const isLastPipelineRunning =
    lastPipeline.status === PipelineStatus.running ||
    lastPipeline.status === PipelineStatus.pending ||
    lastPipeline.status === PipelineStatus.created;
  if (!isLastPipelineRunning) {
    return true;
  }
  return false;
};
