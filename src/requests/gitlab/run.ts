import { executeGitlabReq, GitlabReqMethod } from "./helpers";

export const runPipelineSchedule = async (): Promise<void> => {
  await executeGitlabReq({
    method: GitlabReqMethod.post,
    path: "/play",
  });
};
