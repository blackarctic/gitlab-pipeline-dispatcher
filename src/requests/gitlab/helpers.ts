import axios, { AxiosResponse } from "axios";

import { env } from "../../env";
import { Dictionary } from "../../types";
import { stringifyParams } from "../../utils";

const {
  GITLAB_HOST,
  GITLAB_PROJECT_ID,
  GITLAB_TOKEN,
  GITLAB_PIPELINE_SCHEDULE_ID,
} = env;

const GITLAB_API_URL = `${GITLAB_HOST}/api/v4`;
const GITLAB_PROJECTS_URL = `${GITLAB_API_URL}/projects/${GITLAB_PROJECT_ID}`;
const GITLAB_PIPELINE_SCHEDULE_URL = `${GITLAB_PROJECTS_URL}/pipeline_schedules/${GITLAB_PIPELINE_SCHEDULE_ID}`;

const GITLAB_AUTH_PARAMS = {
  access_token: GITLAB_TOKEN,
};

const createGitlabReqParams = (params: Dictionary) => ({
  ...params,
  ...GITLAB_AUTH_PARAMS,
});

export enum GitlabReqMethod {
  get = "get",
  post = "post",
  delete = "delete",
  put = "put",
}

export const executeGitlabReq = async ({
  params = {},
  path = "",
  method = GitlabReqMethod.get,
}: {
  params?: Dictionary;
  path?: string;
  method?: GitlabReqMethod;
} = {}): Promise<AxiosResponse<any>> => {
  const stringifiedParams = stringifyParams(createGitlabReqParams(params));
  const url = `${GITLAB_PIPELINE_SCHEDULE_URL}${path}?${stringifiedParams}`;
  if (method in GitlabReqMethod) {
    // Tell TS to chill out. All of GitlabReqMethod return the same type.
    return await axios[method as GitlabReqMethod.get](url);
  }
  throw new Error(
    `executeGitlabReq method "${method}" is not valid. Expected "get" or "put".`
  );
};
