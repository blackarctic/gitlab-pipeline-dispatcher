import { PipelineSchedule } from "../../models";
import { executeGitlabReq } from "./helpers";

export const fetchPipelineSchedule = async (): Promise<PipelineSchedule> => {
  const response = await executeGitlabReq();
  const { data } = response;
  if (!data) {
    throw new Error(`No data found for fetchPipelineSchedule`);
  }
  return data;
};
