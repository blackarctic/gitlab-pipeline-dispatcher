import axios from "axios";

import { env } from "../../env";

type MessagePayload = {
  text: string;
  channel?: string;
};

type FormattedMessageAttachment = {
  // This is the title shown in non-formatted contexts (like notifications)
  fallback: string;
  // This is the actual formatted title
  pretext: string;
  // The hex color of the bar to the left of the "fields"
  color: string;
  fields: {
    value: string;
    short: boolean;
  }[];
};

type FormattedMessagePayload = {
  attachments: FormattedMessageAttachment[];
  channel?: string;
};

export const sendMessage = async (message: string, channel?: string) => {
  const payload: MessagePayload = { text: message };
  if (channel) {
    payload.channel = channel;
  }
  await axios.post(env.SLACK_WEBHOOK_URL, payload);
};

export const sendFormattedMessage = async (
  attachments: FormattedMessageAttachment[],
  channel: string
) => {
  const payload: FormattedMessagePayload = { attachments };
  if (channel) {
    payload.channel = channel;
  }
  await axios.post(env.SLACK_WEBHOOK_URL, payload);
};
