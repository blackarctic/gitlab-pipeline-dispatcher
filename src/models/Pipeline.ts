export enum PipelineStatus {
  running = "running",
  pending = "pending",
  success = "success",
  failed = "failed",
  canceled = "canceled",
  skipped = "skipped",
  created = "created",
  manual = "manual",
}

export type Pipeline = {
  id: number;
  sha: string;
  ref: string;
  status: PipelineStatus;
  created_at: string;
  updated_at: string;
  web_url: string;
};

export type PipelineOwner = {
  id: number;
  name: string;
  username: string;
  state: string;
  avatar_url: string;
  web_url: string;
};

export type PipelineEnvVar = {
  variable_type: string;
  key: string;
  value: string;
};

export type PipelineSchedule = {
  id: number;
  description: string;
  ref: string;
  cron: string;
  cron_timezone: string;
  next_run_at: string;
  active: true;
  created_at: string;
  updated_at: string;
  owner: PipelineOwner;
  last_pipeline?: Pipeline;
  variables: PipelineEnvVar[];
};
