import ms from "ms";
import { env } from "./env";
import { run } from "./run";
import { withRejectTimeout } from "./utils";
import { sendMessage } from "./requests/slack";

const {
  SLACK_ERROR_MONIT_CHANNEL,
  REJECT_TIMEOUT_SECONDS,
  INTERVAL_SECONDS,
  FAIL_RETRY_ATTEMPTS,
} = env;

let failCount = 0;

const start = async () => {
  console.log("");
  console.log("Starting...");
  try {
    await withRejectTimeout(run(), "run", ms(`${REJECT_TIMEOUT_SECONDS}s`));
    failCount = 0;
    console.log("Completed successfully");
  } catch (error) {
    failCount += 1;
    console.error(error);
    console.log(`Failed. This makes ${failCount} time(s) in a row.`);
    if (failCount > FAIL_RETRY_ATTEMPTS) {
      console.log("Giving up");
      if (SLACK_ERROR_MONIT_CHANNEL) {
        sendMessage(
          `[Gitlab Pipeline Dispatcher] Help! I have fallen and I can\'t get up. Error is "${error}"`,
          SLACK_ERROR_MONIT_CHANNEL
        );
      }
      throw error;
    }
  }
  console.log(`Will run again in ${INTERVAL_SECONDS} seconds.`);
  setTimeout(() => {
    start();
  }, INTERVAL_SECONDS * 1000);
};

start();
