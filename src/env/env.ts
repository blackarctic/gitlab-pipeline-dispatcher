require("dotenv").config();

import { InjectedEnv, Env } from "./types";

const injectedEnv: InjectedEnv = process.env;

/* Ensure GITLAB_HOST */

const { GITLAB_HOST } = injectedEnv;

if (!GITLAB_HOST) {
  throw new Error("GITLAB_HOST must be set");
}

/* Ensure GITLAB_PROJECT_ID */

if (!injectedEnv.GITLAB_PROJECT_ID) {
  throw new Error("GITLAB_PROJECT_ID must be set");
}

const GITLAB_PROJECT_ID = Number(injectedEnv.GITLAB_PROJECT_ID);

if (Number.isNaN(GITLAB_PROJECT_ID)) {
  throw new Error("GITLAB_PROJECT_ID must be a number");
}

/* Ensure GITLAB_PIPELINE_SCHEDULE_ID */

if (!injectedEnv.GITLAB_PIPELINE_SCHEDULE_ID) {
  throw new Error("GITLAB_PIPELINE_SCHEDULE_ID must be set");
}

const GITLAB_PIPELINE_SCHEDULE_ID = Number(
  injectedEnv.GITLAB_PIPELINE_SCHEDULE_ID
);

if (Number.isNaN(GITLAB_PIPELINE_SCHEDULE_ID)) {
  throw new Error("GITLAB_PIPELINE_SCHEDULE_ID must be a number");
}

/* Ensure GITLAB_TOKEN */

const { GITLAB_TOKEN } = injectedEnv;

if (!GITLAB_TOKEN) {
  throw new Error("GITLAB_TOKEN must be set");
}

/* Ensure SLACK_WEBHOOK_URL */

const { SLACK_WEBHOOK_URL } = injectedEnv;

if (!SLACK_WEBHOOK_URL) {
  throw new Error("SLACK_WEBHOOK_URL must be set");
}

/* Ensure SLACK_ERROR_MONIT_CHANNEL */

const { SLACK_ERROR_MONIT_CHANNEL } = injectedEnv;

if (!SLACK_ERROR_MONIT_CHANNEL) {
  throw new Error("SLACK_ERROR_MONIT_CHANNEL must be set");
}

/* Ensure REJECT_TIMEOUT_SECONDS */

if (!injectedEnv.REJECT_TIMEOUT_SECONDS) {
  throw new Error("REJECT_TIMEOUT_SECONDS must be set");
}

const REJECT_TIMEOUT_SECONDS = Number(injectedEnv.REJECT_TIMEOUT_SECONDS);

if (Number.isNaN(REJECT_TIMEOUT_SECONDS)) {
  throw new Error("REJECT_TIMEOUT_SECONDS must be a number");
}

/* Ensure INTERVAL_SECONDS */

if (!injectedEnv.INTERVAL_SECONDS) {
  throw new Error("INTERVAL_SECONDS must be set");
}

const INTERVAL_SECONDS = Number(injectedEnv.INTERVAL_SECONDS);

if (Number.isNaN(INTERVAL_SECONDS)) {
  throw new Error("INTERVAL_SECONDS must be a number");
}

/* Ensure FAIL_RETRY_ATTEMPTS */

let FAIL_RETRY_ATTEMPTS = Number(injectedEnv.FAIL_RETRY_ATTEMPTS || 0);

if (Number.isNaN(FAIL_RETRY_ATTEMPTS)) {
  FAIL_RETRY_ATTEMPTS = 0;
}

export const env: Env = {
  GITLAB_HOST,
  GITLAB_PROJECT_ID,
  GITLAB_PIPELINE_SCHEDULE_ID,
  GITLAB_TOKEN,
  SLACK_WEBHOOK_URL,
  SLACK_ERROR_MONIT_CHANNEL,
  REJECT_TIMEOUT_SECONDS,
  INTERVAL_SECONDS,
  FAIL_RETRY_ATTEMPTS,
};
