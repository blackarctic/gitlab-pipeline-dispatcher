export const envVarToBool = (s: string | undefined) =>
  !!(s && s.toLowerCase() !== "false");

export const envVarToNumberOrNull = (
  s: string | undefined,
  varName: string
) => {
  if (s === undefined) {
    return null;
  }
  const n = Number(s);
  if (Number.isNaN(n)) {
    throw new Error(`${varName} must be a number if set`);
  }
  return n;
};
