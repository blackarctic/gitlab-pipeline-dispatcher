export type InjectedEnv = {
  GITLAB_HOST?: string;
  GITLAB_PROJECT_ID?: string;
  GITLAB_PIPELINE_SCHEDULE_ID?: string;
  GITLAB_TOKEN?: string;

  SLACK_WEBHOOK_URL?: string;
  SLACK_ERROR_MONIT_CHANNEL?: string;

  REJECT_TIMEOUT_SECONDS?: string;
  INTERVAL_SECONDS?: string;
  FAIL_RETRY_ATTEMPTS?: string;
};

export type Env = {
  GITLAB_HOST: string;
  GITLAB_PROJECT_ID: number;
  GITLAB_PIPELINE_SCHEDULE_ID: number;
  GITLAB_TOKEN: string;

  SLACK_WEBHOOK_URL: string;
  SLACK_ERROR_MONIT_CHANNEL: string;

  REJECT_TIMEOUT_SECONDS: number;
  INTERVAL_SECONDS: number;
  FAIL_RETRY_ATTEMPTS: number;
};
