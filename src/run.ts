import { fetchPipelineSchedule, runPipelineSchedule } from "./requests/gitlab";
import { getShouldStartNewPipeline } from "./selectors";

export const run = async () => {
  const pipelineSchedule = await fetchPipelineSchedule();
  const shouldStartNewPipeline = getShouldStartNewPipeline(pipelineSchedule);

  if (shouldStartNewPipeline) {
    console.log("Dispatching a new pipeline.");
    await runPipelineSchedule();
  } else {
    console.log("No need to dispatch a new pipeline.");
  }
};
